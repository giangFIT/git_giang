<?php

session_start();
require('./CheckAuth.php');

if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $mail = $_POST['mail'];
    $password = $_POST['password'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $passwordConfirm = $_POST['password_confirm'];
    $error = [];

    if (!$mail) {
        $error['mail'] = 'Vui lòng nhập mail';
    } elseif (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $error['mail'] = 'Mail không đúng định dạng';
    } elseif (strlen($mail) > 250) {
        $error['mail'] = 'Độ dài kí tự phải nhỏ hơn 250 kí tự';
    }

    if (!$name) {
        $error['name'] = 'Vui lòng nhập tên';
    } elseif (strlen($name) < 6 || strlen($name) > 100) {
        $error['name'] = 'Độ dài kí tự phải từ 6 đến 100 kí tự';
    } 

    if (!$password) {
        $error['password'] = 'Vui lòng nhập mật khẩu';
    } elseif (strlen($password) < 6 || strlen($password) > 100) {
        $error['password'] = 'Độ dài kí tự phải từ 6 đến 100 kí tự';
    } elseif (!$passwordConfirm) {
        $error['passwordConfirm'] = 'Vui lòng nhập lại mật khẩu';
    } elseif ($passwordConfirm !== $password) {
            $error['passwordConfirm'] = 'Mật khẩu nhập lại không khớp';
    }  
    
    if (strlen($phone) < 10 || strlen($phone) > 20) {
        $error['phone'] = 'Độ dài kí tự phải từ 10 đến 20 kí tự';
    } 

    if (!$address) {
        $error['address'] = 'Vui lòng nhập địa chỉ';
    }

    if (!$error) {
        try {
            $sql = 'Select * from users where mail = :mail';
            $data = array('mail' => $mail);
            $user = $db->queryStatement($db->conn, $sql, $data);

            if ($user) {
                echo 'dữ liệu tồn tại';
            } else {
                $stmt = $db->conn->prepare('INSERT INTO users (`mail`, `name`, `password`, `phone`, `address`) 
                values (:mail, :name, :password, :phone, :address)');                    
                $user = array(
                    'mail'     => $mail,
                    'name'     => $name,
                    'password' => md5($password),
                    'phone'    => $phone,
                    'address'  => $address
                );
                $stmt->execute($user);
                header('location:/LoginPdo.php');
            }
        } catch (PDOException $e) {
            echo $e;
            die;
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <title>Đăng ký</title>
</head>
<body>
    <div class="container" style="margin: 70px auto 0;">
        <form method="post" action="">
            <div class="form-group">
                <label>Email address</label>
                <input type="text" name="mail" class="form-control" value="<?=$mail ?? ''?>">
                <small class="form-text text-danger"><?=$error['mail'] ?? ''?></small>
            </div>

            <div class="form-group">
                <label>Name</label>
                <input type="text" name="name" class="form-control" value="<?=$name ?? ''?>">
                <small class="form-text text-danger"><?=$error['name'] ?? ''?></small>
            </div>

            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?=$password ?? ''?>" >
                <small class="form-text text-danger"><?=$error['password'] ?? ''?></small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password_confirm</label>
                <input type="password" name="password_confirm" class="form-control" value="<?=$passwordConfirm ?? ''?>">
                <small class="form-text text-danger"><?=$error['passwordConfirm'] ?? ''?></small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Phone</label>
                <input type="text" name="phone" class="form-control" value="<?=$phone ?? ''?>"> 
                <small class="form-text text-danger"><?=$error['phone']?></small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Address</label>
                <input type="text" name="address" class="form-control" value="<?=$address ?? ''?>">
                <small class="form-text text-danger"><?=$error['address']?></small>
            </div>
            <button type="submit" name="submit" class="btn btn-primary">Register</button>
        </form>
    </div>
</body>
</html>

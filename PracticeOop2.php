<?php

abstract class Country
{
    protected $slogan;

    /**
     * Set value for object property
     * 
     * @param string $string
     */
    public function setSlogan($string)
    {
        $this->slogan = $string;
    }

    abstract public function sayHello();
}

interface Boss
{
    public function checkValidSlogan();
}

class EnglandCountry extends Country implements Boss
{
    use Active;

    /**
     * say hello
     */
    public function sayHello()
    {
        echo 'Hello';
    }

    /**
     * check input string
     */
    public function checkValidSlogan()
    {
        return strripos($this->slogan, 'england') !== false || strripos($this->slogan, 'english') !== false; 
    }
}

class VietnamCountry extends Country implements Boss
{
    use Active;

    /**
     * say hello
     */
    public function sayHello()
    {
        echo 'Xin chao';
    }

     /**
     * check input string
     */
    public function checkValidSlogan()
    {
        return strripos($this->slogan, 'vietnam') !== false && strripos($this->slogan, 'hust') !== false; 
    }
}

trait Active 
{
    /**
     * get name class
     * 
     * @return string
     */
    public function defindYourSelf()
    {
        return get_class();
    }   
}

$englandCountry = new EnglandCountry();
$vietnamCountry = new VietnamCountry();

$englandCountry->setSlogan('England is a country that is part of the United Kingdom. It shares land borders with Wales to the west and Scotland to the north. The Irish Sea lies west of England and the Celtic Sea to the southwest.');

$vietnamCountry->setSlogan('Vietnam is the easternmost country on the Indochina Peninsula. With an estimated 94.6 million inhabitants as of 2016, it is the 15th most populous country in the world.');

$englandCountry->sayHello(); // Hello
echo '<br>';
$vietnamCountry->sayHello(); // Xin chao
echo '<br>';
var_dump($englandCountry->checkValidSlogan());
echo '<br>';
var_dump($vietnamCountry->checkValidSlogan()); 
echo '<br>';
echo 'I am ' . $englandCountry->defindYourSelf(); 
echo '<br>';
echo 'I am ' . $vietnamCountry->defindYourSelf(); 

<?php

$file1 = 'file1.txt';
$file2 = 'file2.txt';

/**
 * Read file 
 * 
 * @param string $path
 * @return string
 */
function readFile($path) 
{
    $fp = @fopen($path, 'r');//mở file ở chế độ đọc
    if (!$fp) {
        return 'Open file failed';
    }
    $contents = fread($fp, filesize($path));//đọc file
    fclose($fp);
    return $contents;
}

/**
 * Check condition 
 * 
 * @param string $string
 * @return boolean
 */
function checkValidString($string) 
{
    return (!empty($string) && (strpos($string, 'book') !== false xor strpos($string, 'restaurant') !== false));
}

/**
 * Show result of a string 
 * 
 * @param string $path
 * @return string 
 */
function result($path) 
{
    $contents = readFile($path);    
    if (checkValidString($contents)) {
        $sentenceCount = substr_count($contents, '.');
        echo 'Chuỗi hợp lệ, chuỗi gồm ' . $sentenceCount . 'câu';
    } else {
        echo 'Chuỗi không hợp lệ';
    }
}

result($file1);
echo '<br>';
result($file2);

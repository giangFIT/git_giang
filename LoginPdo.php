<?php

session_start();
require('./CheckAuth.php');

if (isset($_POST['submit'])) {
    $mail = $_POST['mail'];
    $password = $_POST['password'];
    $error = [];
    $result = '';

    //Validate email
    if (!$mail) {
        $error['mail'] = 'Vui lòng nhập mail';
    } elseif (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
        $error['mail'] = 'Mail không đúng định dạng';
    } elseif (strlen($mail) > 250) {
        $error['mail'] = 'Độ dài kí tự phải nhỏ hơn 250 kí tự';
    }

    //Validate password
    if (!$password) {
        $error['password'] = 'Vui lòng nhập mật khẩu';
    } elseif (strlen($password) < 6 || strlen($password) > 100) {
        $error['password'] = 'Độ dài kí tự phải từ 6 đến 100 kí tự';
    }

    if (!$error) {
        try {
            $sql = 'Select * from users where mail = :mail and password = :password';
            $data = array(
                'mail' => $mail,
                'password' => md5($password)
            );
            $user = $db->queryStatement($db->conn, $sql, $data);

            if ($user->id) {
                $_SESSION['user'] = [
                    'mail' => $user->mail,
                    'password' => $user->password
                ];
                $_SESSION['result'] = 'Đăng nhập thành công';
                //Remember me
                if (!empty($_POST['remember'])) {
                    setcookie('remember_mail', $mail, time() + 3600);
                    setcookie('remember_pwd', md5($password), time() + 3600);
                }
                header('location:/LoginSuccessPdo.php');
            } else {
                $_SESSION['result'] = 'Đăng nhập thất bại';
            }
        } catch (PDOException $e) {
            echo $e;
            die();
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./vendor/twbs/bootstrap/dist/css/bootstrap.min.css">
    <title>Đăng nhập</title>
    <style>
        .modal-success{
            display: none;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            background-color: #0000001f;
        }

        .content{
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            background-color: #fff;
            border-radius: 5px;
            padding: 60px 0;
            text-align: center;
            width: 600px;
            margin: 20px auto;
        }
    </style>
</head>
<body>
    <div class="container" style="margin: 70px auto 0;">
        <form action="" method="post">
            <div class="mb-3">
                <label class="form-label">Email address</label>
                <input type="text" class="form-control" name="mail">
                <small class="form-text text-danger"><?=$error['mail'] ?? ''?></small>
            </div>
            <div class="mb-3">
                <label class="form-label">Password</label>
                <input type="password" class="form-control" name="password">
                <small class="form-text text-danger"><?=$error['password'] ?? ''?></small>
            </div>
            <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="check1" name="remember">
                <label class="form-check-label" for="check1">remember me</label>
            </div>
            <button type="submit" class="btn btn-primary" name="submit" id="login" >Login</button>
        </form>
    </div>

    <div class="modal-success">
        <div class="content">
            <h3><?=$_SESSION['result']?></h3>
        </div>
    </div>

    <script>
        <?php if ($_SESSION['result'] == 'Đăng nhập thất bại'): ?>
            document.querySelector('.modal-success').style.display = "block";
            document.querySelector('.modal-success').onclick = function () {
                document.querySelector('.modal-success').style.display = "none";
            }
        <?php endif;?>
    </script>
</body>
</html>

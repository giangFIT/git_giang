<?php

class Database
{
    public $conn;

    public function __construct()
    {
        $this->conn = new PDO('mysql:host=db;port=3306;dbname=sampleDB', 'sampleUser', 'password');
    }

    /**
     * Execute statement sql
     * 
     * @param object $conn
     * @param string $sql
     * @param array $data
     * @return object
     */
    public function queryStatement($conn, $sql, $data) 
    {
        $stmt = $conn->prepare($sql);
        $stmt->setFetchMode(PDO::FETCH_OBJ);
        $stmt->execute($data);
        return $stmt->fetch();
    }    
}

$db = new Database;

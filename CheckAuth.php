<?php

require('./Database.php');

class CheckAuth
{
	protected $db;

	public function __construct(Database $database)
	{	
		$this->db = $database;
	}

	/**
	 * check user
	 */
    public function check() 
    {		
		if (!empty($_SESSION['user'])) {
			header('location:/LoginSuccessPdo.php');
		} elseif (!empty($_COOKIE['remember_mail']) && !empty($_COOKIE['remember_pwd'])) {
			try {
				$sql = 'Select * from users where mail = :mail and password = :password';
				$data = array(
					'mail' => $_COOKIE['remember_mail'],
					'password' => $_COOKIE['remember_pwd']
				);
				$user = $this->db->queryStatement($this->db->conn, $sql, $data);

				if ($user->id) {
					$_SESSION['user'] = [
						'mail' => $user->mail,
						'password' => $user->password
					];
					header('location:/LoginSuccessPdo.php');            
				}
			} catch (PDOException $e) {
				echo $e;
				die;
			}
		}	
	} 
}

$checkAuth = new CheckAuth($db);
$checkAuth->check();

<?php

class ExerciseString
{
    public $check1;
    public $check2;

    /**
     * Read file 
     * 
     * @param string $path
     * @return string
     */
    public function readFile($path) 
    {
        $fp = @fopen($path, 'r');
        if (!$fp) {
            return 'Open file failed';
        }
        $contents = fread($fp, filesize($path));
        fclose($fp);
        return $contents;
    }

    /**
     * Check condition 
     * 
     * @param string $string
     * @return boolean
     */
    public function checkValidString($string) 
    {
        return (!empty($string) && (strpos($string, 'book') !== false xor strpos($string, 'restaurant') !== false));
    }

    /**
     * Write data to file
     * 
     * @param string $contentFile2
     * @param boolean $file
     */
    public function writeFile($contentFile2, $file) 
    {
        if (!$file) {
            echo 'Open file failed';
        } else {
            $result1 = $this->check1 ? 'Hợp lệ' : 'Không hợp lệ';
            $result2 = $this->check2 ? 'Hợp lệ' : 'Không hợp lệ';
            $sentenceCount = substr_count($contentFile2, '.');
            $data = sprintf('-  check 1 là chuỗi %s %s-  check 2 là chuỗi %s. Chuỗi có %u câu ', $result1,
            "\n", $result2, $sentenceCount );
            fwrite($file, $data);
            fclose($file);
        }
    }
}
$file = @fopen('result_file.txt', 'w');
$object1 = new ExerciseString;
$contentFile1 = $object1->readFile('file1.txt');
$contentFile2 = $object1->readFile('file2.txt');
$object1->check1 = $object1->checkValidString($contentFile1);
$object1->check2 = $object1->checkValidString($contentFile2);
$object1->writeFile($contentFile2, $file);

-- Create database giang
create database giang;

-- create users table
CREATE TABLE `users` (
  `id` int(6) UNSIGNED AUTO_INCREMENT NOT NULL,
  `mail` varchar(30) NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(30) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `address` varchar(30) DEFAULT NULL,
  `create_at` timestamp NULL,
  `update_at` timestamp NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Insert data
INSERT INTO `users` (`mail`, `name`, `password`, `phone`, `address`) 
VALUES
('giang22.fit@gmail.com', 'giang aa', '123456', '0944837662', 'ha noi');

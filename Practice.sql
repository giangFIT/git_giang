-- Create categories table
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Insert data to categories table
INSERT INTO `categories`
(`id`, `name`, `description`)
VALUES
(1, 'SC', 'Sample Category'),
(2, 'OSC', 'Other Sample Category'),
(3, 'DSC', 'Other Sample Category');

-- Create items table 
CREATE TABLE `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `amount` int(10) NOT NULL,
  `category_id` int(10) NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Insert data to items table
INSERT INTO `items`
(`id`, `name`, `amount`, `category_id`)
VALUES
(1, 'Item 1', 10, 1),
(2, 'Item 2', 20, 1),
(3, 'Item 3', 30, 2),
(4, 'Item 4', 40, 2);

1.
select categories.*, count(items.id) as 'so luong' from categories left join items
on categories.id = items.category_id
group by categories.id;

2. 
select categories.*, sum(items.amount) as 'Tong amout' from categories left join items
on categories.id = items.category_id
group by categories.id;

3.
select categories.* from categories inner join items
on categories.id = items.category_id
where items.amount > 40
group by categories.id;

4.
delete from categories 
where id not in (select items.category_id from items);
